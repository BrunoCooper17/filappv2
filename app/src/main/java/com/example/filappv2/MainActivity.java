package com.example.filappv2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Collections;

import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator;

public class MainActivity extends AppCompatActivity implements RecycleViewClickInterface {

    // Instead of using a list view, a recycler view helps us to get a better performance
    RecyclerView recycleview;
    // The recyclers view logic
    RecyclerAdapter recyclerAdapter;
    // Store the frames in a list
    ArrayList<String> ListFrames;

    // Everytime we start an action we use an identifier to process the output
    private static final int REQUEST_PHOTO = 100;
    private static final int REQUEST_DIRECTORY = 200;
    private static final int REQUEST_ACTIVITY = 300;

    // We need permisions to read externals files
    private static final String[] permissions = {"android.permission.READ_EXTERNAL_STORAGE"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        requestPermissions(permissions, 0);

        // Hide the action bar
        if(getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        // Cache buttons for future use
        FloatingActionButton addPhotoButton = findViewById(R.id.AddPhotoButton);
        FloatingActionButton addDirectoryButton = findViewById(R.id.AddDirectoryButton);
        FloatingActionButton clearButton = findViewById(R.id.ClearButton);
        FloatingActionButton playButton = findViewById(R.id.PlayButton);

        // Open the file manager to select ONE image file
        addPhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.setType("image/*");
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                startActivityForResult(intent, REQUEST_PHOTO);
            }
        });

        // Open the file manager to select a whole directory
        addDirectoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
                startActivityForResult(intent, REQUEST_DIRECTORY);
            }
        });

        // Clear the List of frames and clear the recycler view
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int size = ListFrames.size();
                ListFrames.clear();
                recyclerAdapter.notifyItemRangeRemoved(0, size);

            }
        });

        // Change to the next aciivity. Lets see all the frames one by one in fullscreen
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ListFrames.size() > 0) {
                    //Starting a new Intent
                    Intent nextScreen = new Intent(getApplicationContext(), FramesActivity.class);

                    //Sending data to another Activity
                    nextScreen.putStringArrayListExtra("ListFrames", ListFrames);
                    startActivity(nextScreen);
                }
                else {
                    Toast.makeText(getApplicationContext(), "No images loaded :(",Toast.LENGTH_LONG).show();
                }
            }
        });

        ListFrames = new ArrayList<String>();

        // Inits the recycler view
        recycleview = findViewById(R.id.FrameList);
        recyclerAdapter = new RecyclerAdapter(ListFrames, this);
        recycleview.setLayoutManager(new LinearLayoutManager(this));
        recycleview.setAdapter(recyclerAdapter);

        DividerItemDecoration dividedDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recycleview.addItemDecoration(dividedDecoration);

        // The touch helper helps us to manage the gestures in the recycler view (Sipe to delete and reorder)
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recycleview);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        // In case we open a directory or a single file
        if(requestCode == REQUEST_PHOTO || requestCode == REQUEST_DIRECTORY) {
            Uri path;
            File[] files;
            String newPath;
            if (resultCode == RESULT_OK) {      // The user didn't cancel the operation
                path = resultData.getData();
                newPath = path.getPath();

                // If we read something from the external storage, we ask for the real path for the directory/file
                if (path.toString().contains("externalstorage")) {
                    newPath = Environment.getExternalStorageDirectory().getPath() + "/" + path.getPath().split(":")[1];
                    // Log.d("Files", "Path NEW: " + newPath);
                }
                if (newPath != null) {
                    File dirPath = new File(newPath);
                    switch (requestCode) {
                        // It' just one frame, load it.
                        case REQUEST_PHOTO:
                            ListFrames.add(dirPath.toString());
                            recyclerAdapter.notifyItemInserted(ListFrames.lastIndexOf(dirPath));
                            recyclerAdapter.notifyItemRangeChanged(0, ListFrames.size());
                            break;

                        // We have a directory to load...
                        case REQUEST_DIRECTORY:
                            // Gather all the files with the most popular extentions we know :)
                            files = dirPath.listFiles(new FilenameFilter() {
                                @Override
                                public boolean accept(File dir, String name) {
                                    return name.endsWith(".png") || name.endsWith(".tga") || name.endsWith(".jpg") || name.endsWith(".jpeg");
                                }
                            });
                            // Log.d("FilesToLoad", "Files? : " + files);

                            // There are image files in the directory?
                            if (files != null) {
                                // Log.d("FilesToLoad", "Size: " + files.length);
                                // Store each file in the List of frames
                                for (File file : files) {
                                    // Log.d("FilesToLoad", "FileName:" + file.toString());
                                    ListFrames.add(file.toString());
                                }

                                // Notify the recycler adapter of the change (there ar new frames in the list, load them)
                                int from = ListFrames.lastIndexOf(files[0]);
                                recyclerAdapter.notifyItemRangeInserted(from, files.length);
                                recyclerAdapter.notifyItemRangeChanged(0, ListFrames.size());
                            }
                            break;
                    }
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, resultData);
    }


    ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.LEFT) {
        // Manages the reposition of items in the recycler view
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            int fromPosition = viewHolder.getAdapterPosition();
            int toPosition = target.getAdapterPosition();

            Collections.swap(ListFrames, fromPosition, toPosition);

            recyclerAdapter.notifyItemMoved(fromPosition, toPosition);
            recyclerAdapter.notifyItemChanged(fromPosition);
            recyclerAdapter.notifyItemChanged(toPosition);

            return false;
        }

        // Lets us handle the swipe event to delete one item of the recycler view
        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
            int position = viewHolder.getAdapterPosition();
            switch (direction) {
                case ItemTouchHelper.LEFT:
                    ListFrames.remove(position);
                    recyclerAdapter.notifyItemRemoved(position);
                    recyclerAdapter.notifyItemRangeChanged(0, ListFrames.size());
                    break;
            }
        }

        // Make use of an external library to change the color of the item when the user swipe to left
        @Override
        public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
            new RecyclerViewSwipeDecorator.Builder(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                    .addSwipeLeftBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorDelete))
                    .addActionIcon(R.drawable.ic_delete_black_24dp)
                    .create()
                    .decorate();
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        }
    };

    @Override
    public void OnClickItem(int position) {
//        Toast.makeText(this, "Pressed: " + (position+1), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void OnLongClickItem(int position) {
//        recyclerAdapter.notifyItemRemoved(position);
//        recyclerAdapter.notifyItemRangeChanged(position, recyclerAdapter.getItemCount());
    }


    // Change the layout from Landscape to portrait and viceversa, the whole activity reloads.
    // So before that, the list of frames is store and restore.
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putStringArrayList("ListFrames", ListFrames);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        ListFrames = savedInstanceState.getStringArrayList("ListFrames");
//        Log.i("RESTORE", "ListFrame Size?: " + ListFrames.size());
        recyclerAdapter.setList(ListFrames);
        recyclerAdapter.notifyItemRangeInserted(0, ListFrames.size());
        super.onRestoreInstanceState(savedInstanceState);
    }
}
