package com.example.filappv2;

public interface RecycleViewClickInterface {
    void OnClickItem(int position);
    void OnLongClickItem(int position);
}
