package com.example.filappv2;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.io.File;
import java.util.List;
import java.util.ListResourceBundle;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private static final String TAG = "RecyclerAdapter";
    private RecycleViewClickInterface recycleClickEventsInterfaces;

    private List<String> listRecycle;

    public RecyclerAdapter(List<String> list, RecycleViewClickInterface recycleClickEventsInterfaces) {
        setList(list);
        this.recycleClickEventsInterfaces = recycleClickEventsInterfaces;
    }

    public void setList(List<String> list) {
        listRecycle = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.row_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        File tmpFile = new File(listRecycle.get(position));
        String frameIndex = String.valueOf(position+1) + "/" +  getItemCount();
        holder.indexTextView.setText(frameIndex);
        holder.nameTextView.setText(tmpFile.getName());
        Glide.with(holder.itemView.getContext()).load(tmpFile).into(holder.thumbnailView);
    }

    @Override
    public int getItemCount() {
        return listRecycle.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView thumbnailView;
        TextView nameTextView;
        TextView indexTextView;

        public ViewHolder (@NonNull View itemView) {
            super(itemView);

            thumbnailView = itemView.findViewById(R.id.thumbnailView);
            nameTextView = itemView.findViewById(R.id.textName);
            indexTextView = itemView.findViewById(R.id.frameNumber);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    recycleClickEventsInterfaces.OnClickItem(getAdapterPosition());
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    recycleClickEventsInterfaces.OnLongClickItem(getAdapterPosition());
                    return true;
                }
            });
        }
    }
}
