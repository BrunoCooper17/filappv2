package com.example.filappv2;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.io.File;
import java.util.ArrayList;

public class FramesActivity extends AppCompatActivity {

    ArrayList<String> listFrames;

    ImageView imageView;
    TextView frameIndexView;
    int index = 0;

    // If the button actions need to be remapped, just change it here.
    private static final int ACTION_PREVIOUS   = KeyEvent.KEYCODE_DPAD_LEFT;
    private static final int ACTION_NEXT       = KeyEvent.KEYCODE_DPAD_RIGHT;
    private static final int ACTION_SHOW_INDEX = KeyEvent.KEYCODE_BUTTON_A;
    private static final int ACTION_GO_BACK    = KeyEvent.KEYCODE_BUTTON_B;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frames);

        // Hide the action bar
        if(getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        // Gets the list of frames sended from the other activity
        Intent intent = getIntent();
        listFrames = intent.getStringArrayListExtra("ListFrames");

        // Cache used components
        imageView = findViewById(R.id.FrameView);
        frameIndexView = findViewById(R.id.FrameIndexView);

        // Set the app in fullscreen mode
        frameIndexView.setVisibility(View.INVISIBLE);
        imageView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        index = 0;
        LoadImage();
    }

    // Move to next frame
    protected void NextPicture() {
        if(listFrames != null) {
            index++;
            if(index >= listFrames.size()) index = 0;
            LoadImage();
        }
    }

    // Move to previous frame
    protected void PrevPicture() {
        if(listFrames != null) {
            index--;
            if(index < 0) index = listFrames.size() -1;
            LoadImage();
        }
    }

    // Load the current frame using the current index
    protected void LoadImage() {
        frameIndexView.setText("Frame " + (index+1) + "/" + listFrames.size());
        Glide.with(imageView.getContext())
                .load(new File(listFrames.get(index))) // Uri of the picture
                .dontTransform()
                .into(imageView);
    }


    // Gamepad events
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if(event.getAction() == KeyEvent.ACTION_DOWN) {
//            Log.d("Input", "KeyEvent: " + event.toString());
            if (event.getKeyCode() == ACTION_PREVIOUS) {
//                Log.d("Input", "PRESS DPAD LEFT");
                PrevPicture();
            }
            if (event.getKeyCode() == ACTION_NEXT) {
//                Log.d("Input", "PRESS DPAD RIGHT");
                NextPicture();
            }
            if (event.getKeyCode() == ACTION_SHOW_INDEX) {
                frameIndexView.setVisibility(View.VISIBLE);
            }
            if (event.getKeyCode() == ACTION_GO_BACK || event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
//                Log.d("Input", "PRESS BUTTON START");
                finish();
            }
        }
        else if(event.getAction() == KeyEvent.ACTION_UP) {
            if (event.getKeyCode() == ACTION_SHOW_INDEX) {
                frameIndexView.setVisibility(View.INVISIBLE);
            }
        }
        return true;
        //return super.dispatchKeyEvent(event);
    }



    // Change the layout from Landscape to portrait and viceversa, the whole activity reloads.
    // So before that, the list of frames and current index are store and restore.
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putStringArrayList("ListFrames", listFrames);
        outState.putInt("index", index);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRestoreInstanceState(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        listFrames = savedInstanceState.getStringArrayList("ListFrames");
        index = savedInstanceState.getInt("index");
        LoadImage();

        super.onRestoreInstanceState(savedInstanceState, persistentState);
    }
}
